# User Guide - Linux

## Limitations

* **Hibernation is not supported and blocked.** Limited hibernation support might be possible in the future. As there are no real use cases, support is not planned.
* **Reloading the driver** is not supported. As there are no real use cases, support is not planned.
* **Defragmentation** to optimize the Linux memory block layout over time is not implemented yet.
* **Memory onlining inside the Linux guest still has to be configured manually via the Linux kernel command line** when intending to use a different [memory onlining policy](user-guide-linux.md#quick-facts) than onlining all memory to ZONE_NORMAL. A systemd unit to make configuration easier is in the works.

## Updates

### v6.10
* The virtio-mem device driver now supports [suspend+resume](https://lore.kernel.org/linux-kernel/20240318120645.105664-1-david@redhat.com/T/). s2idle and suspending without plugged memory is always supported. Suspending to ACPI S3 with plugged memory requires a new device feature, and is blocked if the device does not support it.

### v5.19

* Thanks to Zi Yan, virtio-mem now supports memory hot(un)plug in
[pageblock granularity](https://lore.kernel.org/lkml/20220425143118.2850746-1-zi.yan@sent.com/)
-- for example, 2 MiB on x86-64 and aarch64 (instead of previously 4 MiB).
This increases the usualy amount of memory that can get hotunplugged if the
ZONE_MOVABLE is not used.

### v5.18

* Thanks to Gavin Shan, virtio-mem now supports (aarch64)[https://lore.kernel.org/linux-arm-kernel/20220119010551.181405-1-gshan@redhat.com/].

### v5.16

* virtio-mem device memory can no longer be mapped to user space directly, for example, [via /dev/mem](https://lkml.kernel.org/r/20210920142856.17758-1-david@redhat.com), to prohibit accidential access to unplugged device memory.
* [/proc/vmcore access can now be sanitized by the virtio-mem kernel module in the kdump kernel](https://lkml.kernel.org/r/20211005121430.30136-1-david@redhat.com), making it possible to dump virtio-mem memory without tools like makedumpfile cleanly. **Distributions supporting virtio-mem should make sure to include the virtio-mem kernel module in the kdump initrd -- which is automatically done with dracut >= 054**.&#x20;
* Linux now supports the [VIRTIO_MEM_F_UNPLUGGED_INACCESSIBLE feature](https://lkml.kernel.org/r/20211006122709.27885-1-david@redhat.com). Some hypervisors will soon require this feature, for example, when backing a virtio-mem device by huge pages or other scarce resources that don't provide shared zeropages.
* The [memory hot(un)plug documentation in Linux has been extended](https://www.kernel.org/doc/html/latest/admin-guide/mm/memory-hotplug.html) to properly document the auto-movable memory onlining policy.

### v5.15

* Linux now supports the [auto-movable memory onlining policy](https://lkml.kernel.org/r/20210806124715.17090-1-david@redhat.com), making it possible grow a VM big but still being able to reliably shrink it to some degree, as defined by a target ratio, later again reliably. Hotplugged virtio-mem memory will be onlined to ZONE_MOVABLE if the current zone ratio allows for it, otherwise to ZONE_NORMAL. See [memory onlining](user-guide-linux.md#quick-facts) for details.
* The [memory hot(un)plug documentation in Linux has been overhauled](https://www.kernel.org/doc/html/latest/admin-guide/mm/memory-hotplug.html), especially also including a proper documentation_f ZONE_MOVABLE.

### v5.14

* [/proc/kcore has been reworked](https://lkml.kernel.org/r/20210526093041.8800-1-david@redhat.com) to no longer read unplugged memory located on a virtio-mem device. Another preparation for cleanly supporting more memory backends (shmem, hugetlbfs, files) in the hypervisor.
* [virtio-mem will prioritize unplug from ZONE_MOVABLE](https://lkml.kernel.org/r/20210602185720.31821-1-david@redhat.com), preparing it for onlining hotplugged device memory to mixed zones, which will be possible with optimized auto-onlining of memory.

### v5.13

* alloc_contig_range() now supports [migration of free/busy hugetlb pages](https://lkml.kernel.org/r/20210419075413.1064-1-osalvador@suse.de), turning virtio-mem+ZONE_MOVABLE+hugetlb fully usable.
* [/dev/kmem has been removed](https://lwn.net/Articles/851531/), getting rid of one interface where the Linux kernel could have been tricked into reading unplugged memory located on a virtio-mem device. This is a preparation for cleanly supporting more memory backends (shmem, hugetlbfs, files) in the hypervisor.
* [kdump on x86-64 will now also dump memory](https://lkml.kernel.org/r/20210325115326.7826-1-david@redhat.com) provided by virtio-mem devices. [kexec-tools has been updated](https://lkml.kernel.org/r/20210323100111.8365-1-david@redhat.com) as well.

### v5.11

* virtio-mem now supports [Big Block Mode](https://lkml.kernel.org/r/20201012125323.17509-1-david@redhat.com) and can handle device block sizes that exceed the size of a Linux memory block. For example, while the Linux memory block size might be 128 MiB, the virtio-mem device block size might be 1 GiB due to gigantic pages in the hypervisor: this can now be handled properly by the Linux driver.

### v5.10

* virtio-mem now support [ZONE_MOVABLE](https://lore.kernel.org/linux-mm/20200804194142.28279-1-david@redhat.com/), which highly improves memory hotunplug capabilities. **Be sure to only enable this when not hotplugging too much memory, otherwise it can be dangerous - if you're planning on hotplugging more than 3-4 times the initial/boot memory, don't use this!**
  * Via the kernel cmdline _memhp_default_state=online_movable_
  * Via _"echo online_kernel > /sys/devices/system/memory/auto_online_blocks"_ during/after boot
* [System RAM resource merging](https://lore.kernel.org/linux-mm/20200911103459.10306-1-david@redhat.com/). Improves performance when hotplugging a lot of memory and compresses /proc/iomem output.
* [Page allocator optimizations](https://lore.kernel.org/linux-acpi/20201005121534.15649-1-david@redhat.com/) to avoid allocating just-onlined memory. Increases the amount of memory that can get hotunplugged when relying on ZONE_NORMAL.

## Memory Onlining

Memory hotplugged to Linux has to be onlined for it to be usable by the Linux page allocator. Added Linux memory blocks can either be onlined from user space (e.g., via udev rules) or automatically from the Linux kernel. virtio-mem will wait with hotplugging more memory if previously hotplugged memory doesn't get onlined, to avoid triggering out of memory situations.

**The recomendation is to configure automatic memory onlining in the kernel**, especially, because virtio-mem memory hotplug performance might be degraded otherwise. While a runtime configuration is possible via the sysfs, **the suggestion for now is to configure automatic memory onlining using the kernel command line**; this way, one doesn't have to bother about manually onlining memory that was hotplugged before the runtime configuration was active.

When configuring memory onlining, a memory online policy has to be selected. The policy implies memory hotunplug reliability, but also how much memory can be safely hotplugged.

More details and considerations, especially on ZONE_MOVABLE usage, can be found in the [Linux kernel memory hot(un)plug documentation](https://www.kernel.org/doc/html/latest/admin-guide/mm/memory-hotplug.html).

### Kernel / ZONE_NORMAL

When onlining hotplugged memory to ZONE_NORMAL, **there are no real guarantees how much memory can get hotunplugged again later**. However, **one can grow a VM very big** without eventually harming the system.

To configure automatic memory onlining to ZONE_NORMAL, add the following to the kernel command line and reboot:

```
memhp_default_state=online_kernel
```

### Movable / ZONE_MOVABLE

When onlining hotplugged memory to ZONE_MOVABLE, **ignoring corner cases, all hotplugged memory can get hotunplugged fairly reliable later**. However, **care has to be taken to not create** [**zone imbalanes**](https://www.kernel.org/doc/html/latest/admin-guide/mm/memory-hotplug.html#zone-imbalances), having too much movable vs. kernel memory in the system and eventually crashing the system or harming the workload. See the [ZONE_MOVABLE sizing considerations](https://www.kernel.org/doc/html/latest/admin-guide/mm/memory-hotplug.html#zone-movable-sizing-considerations).

Often, MOVABLE:KERNEL ratios of up to 3:1 or even 4:1 are fine -- for example, hotplugging 24 GiB to a 8 GiB VM and onlining all htoplugged memory to ZONE_MOVABLE.

To configure automatic memory onlining to ZONE_MOVABLE, add the following to the kernel command line and reboot:

```
memhp_default_state=online_movable
```

### auto-movable / ZONE_NORMAL + ZONE_MOVABLE

The auto-movable policy **tries to avoid creating zone imbalances but still tries to make as much memory as possible reliably hotunpluggable**. Based on the current MOVABLE:KERNEL ratio and a user-defined target ratio, it conditionally onlines hotplugged memory either to ZONE_NORMAL or ZONE_MOVABLE. Please note a mis-configuration for the target workload can still harm the system.

To configure the auto-movable policy (here, using a ratio of 301 %), add the following to the kernel command line and reboot:

```
memory_hotplug.online_policy=auto-movable memory_hotplug.auto_movable_ratio=301 memhp_default_state=online
```

More details can be found in the [Linux kernel memory hot(un)plug documentation](https://www.kernel.org/doc/html/latest/admin-guide/mm/memory-hotplug.html).

## Imprecise Memory Statistics
Some memory statistics are imprecise and don't accurately reflect the memory currently provided by virtio-mem devices. A similar behavior is known from memory ballooning.

The `lsmem` command under Linux only counts the number of "Memory blocks" represented as directories in `/sys/devices/system/memory`, to compute "Total online" and "Total offline" memory. As virtio-mem can add/remove memory within such fixed-sized memory blocks in smaller chunks, the currently available memory in the system can be __smaller__ than this number. If virtio-mem is using the "Big Block Mode", this does not apply.

Similarly "present" and "spanned" pages in `/proc/zoneinfo` per zone might over-indicate memory added by virtio-mem. However, "managed" pages correctly reflects virtio-mem memory.

A reliable source that correctly includes actually used virtio-mem memory is "MemTotal" in `/proc/meminfo` or `/sys/devices/system/node/nodeX/meminfo` (for each node X).

## tmpfs

In general, tmpfs mounts are not resized automatically when hot(un)plugging memory. Also, tmpfs mounts created during boot (whereby the tmpfs size is based on the initial RAM size) won't consider virtio-mem memory. Any tmpfs mount can be resized using _mount_ with the _remount_ option_._ For example, to resize the _/dev/shm/_ mount from 3.7G to 8G:

```
$ df -ah | grep /dev/shm
tmpfs           3.7G     0  3.7G   0% /dev/shm
$ mount -o remount,size=8G tmpfs /dev/shm/
$ df -ah | grep /dev/shm
tmpfs           8.0G     0  8.0G   0% /dev/shm
```

Watch out to [not oversize tmpfs](https://www.kernel.org/doc/html/latest/filesystems/tmpfs.html), especially when unplugging memory.
