---
description: Paravirtualized memory hot(un)plug
---

# virtio-mem

## Project Goals

The main goal of virtio-mem is to allow for dynamic resizing of virtual machine memory. virtio-mem provides a flexible, cross-architecture memory hot(un)plug solution that avoids many limitations imposed by existing technologies, architectures, and interfaces: paravirtualized memory hot(un)plug.

On x86-64 and aarch64, memory can currently be added/removed from a virtual machine running Linux in 2 MiB granularity.

[Features](user-guide/#features) and [limitations](user-guide/#limitations) can be found in the [user guide](user-guide/).

## Architecture Support

x86-64, aarch64 and RISC-V are supported. s390x support is under active development. Support for 32-bit architectures is not planned.

## Guest Operating System Support

Linux guests are fully supported. Support for Windows guests [has been added](https://github.com/virtio-win/kvm-guest-drivers-windows/pull/1062) to [virtio-win](https://github.com/virtio-win), but is still under development and should be consider unstable (technology-preview).

## Additional Sources of Information

* "[virtio-mem: paravirtualized memory hot(un)plug](https://dl.acm.org/doi/abs/10.1145/3453933.3454010)" conference paper in VEE'21
* "[virtio-mem goes to Windows](https://www.youtube.com/watch?v=I\_XvMBHzmDw)" talk at DevConf.CZ 2021
* "Virtio-(balloon|pmem|mem): Managing Guest Memory" talk at KVM Forum 2020: [Video](https://www.google.com/url?sa=t\&rct=j\&q=\&esrc=s\&source=web\&cd=\&cad=rja\&uact=8\&ved=2ahUKEwi-yKOClJfxAhWFyaQKHZGcCEUQtwIwA3oECAkQAw\&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DFq47WCCm-HM\&usg=AOvVaw0ijc4wGrkRRnHJ51DqhBO7) and [slides](https://static.sched.com/hosted_files/kvmforum2020/8e/KVM%20Forum%202020%20Virtio-%28balloon%20pmem%20mem%29%20Managing%20Guest%20Memory.pdf)
* "virtio-mem: Paravirtualized Memory" talk at KVM Forum 2018: [Video ](https://www.youtube.com/watch?v=H65FDUDPu9s)and [slides](https://events19.linuxfoundation.org/wp-content/uploads/2017/12/virtio-mem-Paravirtualized-Memory-David-Hildenbrand-Red-Hat-1.pdf)
* [Linux cover letter](https://lore.kernel.org/kvm/20200311171422.10484-1-david@redhat.com/) and [Linux main patch](https://lore.kernel.org/r/20200507140139.17083-2-david@redhat.com)
* [QEMU main patch](https://lkml.kernel.org/r/20200626072248.78761-11-david@redhat.com)

On questions, you can drop David Hildenbrand a mail (see Linux/QEMU links above for the mail address).

## Repositories

#### Kernel

[Linux guests](https://www.kernel.org) support virtio-mem since v5.8-rc1.

#### QEMU

[QEMU](https://www.qemu.org) supports virtio-mem since v5.1.0-rc1.

#### libvirt

[libvirt](https://gitlab.com/libvirt/libvirt) supports virtio-mem since v7.9.0-rc1.

#### virtio-spec

virtio-mem is specified in [virtio-v1.2](https://docs.oasis-open.org/virtio/virtio/v1.2/cs01/virtio-v1.2-cs01.pdf) as "Memory Device".

#### cloud-hypervisor

[Upstream cloud-hypervisor](https://github.com/cloud-hypervisor/cloud-hypervisor) supports virtio-mem.

#### virtio-win

[virtio-win](https://github.com/virtio-win) added support for virtio-mem as tech-preview, but there is no public release that contains the changes yet.
